package config

import (
	"fmt"
	"github.com/casbin/casbin"
	"github.com/casbin/gorm-adapter"
)

var Enfocer  *casbin.Enforcer

func GetPermissionModel()  {


	policy, err := gormadapter.NewAdapterByDBUseTableName(Db, "", "permission")
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Unable to load database: %v \n", err))
	}
	Enfocer, err = casbin.NewEnforcer("./rbac_model.conf", policy)
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Unable to load conf file: %v \n", err))
	}

}