package config

import (
	"fmt"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"reflect"

	"github.com/labstack/echo"
)

type Response struct {
	IsError    bool
	ErrMessage interface{}
	Data       interface{}
}

type Pagination struct {
	Current_page interface{} `json:"current_page"`
	Per_page     interface{} `json:"per_page"`
	Total        interface{} `json:"total"`
	Data         interface{} `json:"data"`
}

func PageInfo(data interface{}, vPage int, vLimit int, vTotal int64) interface{} {

	var res interface{}
	if !reflect.ValueOf(data).IsNil() {
		res = Pagination{
			Current_page: vPage,
			Per_page:     vLimit,
			Total:        vTotal,
			Data:         data,
		}
	}

	return res
}

func Respon(c echo.Context, isError bool, err error, data interface{}) error {

	var errMessage interface{}

	if isError {
		errMessage = err.Error()

		if castedObject, ok := err.(validator.ValidationErrors); ok {
			for _, err := range castedObject {
				switch err.Tag() {
				case "required":
					errMessage = fmt.Sprintf("%s is required",
						err.Field())
				case "email":
					errMessage = fmt.Sprintf("%s is not valid email",
						err.Field())
				case "gte":
					errMessage = fmt.Sprintf("%s value must be greater than %s",
						err.Field(), err.Param())
				case "lte":
					errMessage = fmt.Sprintf("%s value must be lower than %s",
						err.Field(), err.Param())
				}

				break
			}
		}
	}

	var res = Response{}
	res.IsError = isError
	res.ErrMessage = errMessage
	res.Data = data

	return c.JSON(http.StatusOK, res)
}
