package service

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"meta-rent/config"
	"meta-rent/model"
	"meta-rent/request"
	"strconv"
)

func LoginService(param *request.Login) (data interface{}, err error) {

	email := param.Email
	password := param.Password

	var user model.User

	db := config.Db
	_ = db.
		Where("email = ?", email).
		First(&user)

	if &user != nil {
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
		if err == nil {

			token := jwt.New(jwt.SigningMethodHS256)
			claims := token.Claims.(jwt.MapClaims)
			claims["name"] = user.Name
			claims["id"] = strconv.Itoa(user.Id)

			t, err := token.SignedString([]byte("secret"))
			if err != nil {
				return "fatal", err
			}

			return t, nil

		} else {
			return nil, errors.New("wrong password")
		}
	} else {
		return nil, errors.New("email not found")
	}

}

func RegisterService(param *model.User) (data interface{}, err error) {

	db := config.Db

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	user := model.User{
		Name:     param.Name,
		Email:    param.Email,
		Password: string(hashedPassword),
		Is_active: 1,
	}

	res := db.Create(&user)
	if res.Error != nil {
		return nil, res.Error
	}

	e := config.Enfocer

	role, err := e.AddRoleForUser(strconv.Itoa(user.Id), "store_owner")
	if err != nil {
		return nil, err
	}

	fmt.Println(role)

	return user, nil
}

func GetProfileService(userId int) (response model.User, err error) {

	db := config.Db

	var data model.User
	resp := db.
		First(&data, userId).
		Model(&data)

	if &data != nil {
		e := config.Enfocer
		roles, _ := e.GetRolesForUser(strconv.Itoa(userId))
		data.Role = &roles
	}

	if resp.Error != nil {
		return data, resp.Error
	}

	return data, nil
}