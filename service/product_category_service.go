package service

import (
	"meta-rent/config"
	"meta-rent/model"
	"meta-rent/request"
)

func ProductCategoryListService(param *request.List) (resp interface{}, total int64, err error) {

	where := "id IS NOT NULL"
	if param.UserId != nil {
		where += " AND product_categories.user_id = " + *param.UserId
	}

	var data []model.ProductCategory
	db := config.Db
	db = db.
		Model(&data).
		Where(where).
		Count(&total).
		Scopes(config.DbPaginate(param.Page, param.Limit, param.Sort)).
		Find(&data)

	return data, total, nil
}

func ProductCategoryGetService(id int) (data model.ProductCategory, err error) {

	db := config.Db
	db.Joins("Story").
		First(&data, id)

	return data, nil
}

func ProductCategoryAddService(param *model.ProductCategory) (data interface{}, err error) {

	db := config.Db

	store := param

	row := new(model.ProductCategory)
	res := db.Create(&store).Scan(&row)
	if res.Error != nil {
		return nil, res.Error
	}

	return store, nil

}

func ProductCategoryUpdateService(param *model.ProductCategory) (data interface{}, err error) {

	db := config.Db

	var store model.ProductCategory
	db.First(&store, param.Id)
	res := db.Model(&store).Updates(param)

	if res.Error != nil {
		return nil, err
	}

	return config.FormatResUpdate(res.RowsAffected), nil
}

func ProductCategoryDeleteService(id int) (data interface{}, err error) {

	db := config.Db
	res := db.Delete(&model.ProductCategory{}, id)

	if res.Error != nil {
		return nil, res.Error
	}

	return config.FormatResDelete(res.RowsAffected), nil
}
