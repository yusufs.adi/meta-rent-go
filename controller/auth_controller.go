package controller

import (
	"github.com/labstack/echo"
	errors "golang.org/x/xerrors"
	"meta-rent/config"
	"meta-rent/middleware"
	"meta-rent/model"
	"meta-rent/request"
	"meta-rent/service"
)

func Register(c echo.Context) error {

	param := new(model.User)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, errors.New("Binding data error"), nil)
	}

	data, err := service.RegisterService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func Login(c echo.Context) error {

	param := new(request.Login)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	if err := c.Validate(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	data, err := service.LoginService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func Profile(c echo.Context) error {

	data, err := service.GetProfileService(middleware.AuthUser.Id)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}