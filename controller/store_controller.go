package controller

import (
	"github.com/labstack/echo"
	errors "golang.org/x/xerrors"
	"meta-rent/config"
	"meta-rent/middleware"
	"meta-rent/model"
	"meta-rent/request"
	"meta-rent/service"
	"strconv"
)

func StoreList(c echo.Context) error {

	param := new(request.List)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, errors.New("Binding data error"), nil)
	}

	data, total, err := service.StoreListService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	resp := config.PageInfo(data, param.Page, param.Limit, total)

	return config.Respon(c, false, nil, resp)

}

func StoreGet(c echo.Context) error {

	id, err := strconv.Atoi(c.Param("id"))
	data, err := service.StoreGetService(id)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func StoreAdd(c echo.Context) error {

	param := new(model.Store)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, errors.New("Binding data error"), nil)
	}

	if err := c.Validate(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	e := config.Enfocer
	res, _ := e.HasRoleForUser(strconv.Itoa(middleware.AuthUser.Id), "store_owner")
	if res {
		param.UserId = middleware.AuthUser.Id
	}

	data, err := service.StoreAddService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func StoreUpdate(c echo.Context) error {

	param := new(model.Store)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	data, err := service.StoreUpdateService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func StoreDelete(c echo.Context) error {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	data, err := service.StoreDeleteService(id)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}
