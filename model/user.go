package model

import (
	"gorm.io/gorm"
	"time"
)

func (u *User) TableName() string {
	return "users"
}

type User struct {
	Id        int            `json:"id"`
	Name      string         `json:"name"`
	Email     string         `json:"email"`
	Password  string         `json:"password"`
	Is_active int            `json:"is_active"`
	CreatedAt time.Time      `json:"created"`
	CreatedBy int            `json:"created_by"`
	UpdatedAt time.Time      `json:"updated"`
	UpdatedBy int            `json:"updated_by"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
	Role      *[]string      `json:"role" gorm:"-"`
}
