package model

import (
	"gorm.io/gorm"
	"time"
)

func (u *Store) TableName() string {
	return "stores"
}

type Store struct {
	Id          int            `json:"id"`
	UserId      int            `json:"user_id"`
	Name        string         `json:"name"`
	Address     string         `json:"address"`
	Description *string        `json:"description"`
	Lat         *float64       `json:"lat"`
	Lng         *float64       `json:"lng"`
	Image       *string        `json:"image"`
	CreatedAt   time.Time      `json:"created_at"`
	CreatedBy   int            `json:"created_by"`
	UpdatedAt   time.Time      `json:"updated_at"`
	UpdatedBy   int            `json:"updated_by"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
	User        *User          `json:"user" gorm:"foreignKey:UserId"`
}
