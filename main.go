package main

import (
	"meta-rent/config"
	"meta-rent/controller"
	middle "meta-rent/middleware"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
)

func main() {

	// Load Config
	config.ReadConfig()

	// Load Database
	config.DbConnect()
	sqlDB, _ := config.Db.DB()
	defer sqlDB.Close()

	// Load Permission
	config.GetPermissionModel()

	// Route
	e := echo.New()
	e.Validator = &middle.CustomValidator{Validator: validator.New()}
	e.Use(middle.Logging)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	e.POST("register", controller.Register)
	e.POST("login", controller.Login)

	rest := e.Group("/api", middleware.JWT([]byte("secret")), middle.Auth)
	rest.GET("/profile", controller.Profile)

	rest.GET("/store", controller.StoreList)
	rest.GET("/store/:id", controller.StoreGet)
	rest.POST("/store", controller.StoreAdd)
	rest.PUT("/store", controller.StoreUpdate)
	rest.DELETE("/store/:id", controller.StoreDelete)

	rest.GET("/product_category", controller.ProductCategoryList)
	rest.GET("/product_category/:id", controller.ProductCategoryGet)
	rest.POST("/product_category", controller.ProductCategoryAdd)
	rest.PUT("/product_category", controller.ProductCategoryUpdate)
	rest.DELETE("/product_category/:id", controller.ProductCategoryDelete)

	e.Any("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, c.Request().Method+" :: "+config.App.AppName)
	})

	// Start Server
	e.Logger.Fatal(e.Start(":" + config.App.Server.Port))

}
