package middleware

import (
	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
	"time"
)

func Logging(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		startTime := time.Now()
		err := next(c)
		duration := int64(time.Now().Sub(startTime) / time.Millisecond)
		MakeLogEntry(c, duration).Info("incoming request")
		return err
	}
}

func MakeLogEntry(c echo.Context, d int64) *log.Entry {
	if c == nil {
		return log.WithFields(log.Fields{
			"at": time.Now().Format("2006-01-02 15:04:05"),
		})
	}

	return log.WithFields(log.Fields{
		"at":     time.Now().Format("2006-01-02 15:04:05"),
		"method": c.Request().Method,
		//"device": c.Request().Header.Get("User-Agent"),
		"uri":    c.Request().URL.String(),
		"ip":     c.Request().RemoteAddr,
		"status": c.Response().Status,
		"ms":     d,
	})
}
